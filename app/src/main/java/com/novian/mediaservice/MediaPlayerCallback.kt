package com.novian.mediaservice

interface MediaPlayerCallback {
    fun onPlay()
    fun onStop()
}
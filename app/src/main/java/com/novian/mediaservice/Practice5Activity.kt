package com.novian.mediaservice

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.novian.mediaservice.R
import com.novian.mediaservice.Practice5MediaPlayerServiceFragment
import kotlinx.android.synthetic.main.activity_practice5.*

class Practice5Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practice5)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        goToMediaPlayerService("Media Player Service")
    }



    fun goToMediaPlayerService(title:String){
        supportActionBar?.title = title
        val mFragmentManager = supportFragmentManager
        val myFragment = Practice5MediaPlayerServiceFragment()
        val fragment = mFragmentManager.findFragmentByTag(Practice5MediaPlayerServiceFragment::class.java.simpleName)
        if (fragment !is Practice5MediaPlayerServiceFragment) {
            mFragmentManager
                .beginTransaction()
                .replace(R.id.frame_container, myFragment, Practice5MediaPlayerServiceFragment::class.java.simpleName)
                .commit()
        }
    }

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            val fragment: Fragment
            val bundle: Bundle
            when (item.itemId) {

                R.id.nav_media_player_service -> {
                    goToMediaPlayerService("Media Player Service")
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }
}
